<html>

<!-- gère l'affichage des médicaments par mois -->


<?php
require_once 'includes/head.php'; // c'est dans ce fichier qu'on a le lien vers style.css
require_once './mesClasses/Cpresenter.php';
require_once './mesClasses/Cvisiteurs.php';
require_once 'nav.php';


$oemploye = unserialize($_SESSION['visitauth']);

?>


<body>

    <?php

    $omedicaments = new CmedaPresenters(); // je crée un nouvel objet de type CmedaPresenters
    $ocoll = $omedicaments->ocollmedicament;
    ?>

    <!-- html -->
    <div class="container">

        <header title="listevisiteur"></header>
        <h1>
            <p title="tabvisiteur">Liste des Médicaments à présenter pour
                <?php
                setlocale(LC_TIME, "fr_FR", "French"); // sert a mettre le mois en francais
                echo strftime("%B");
                echo (" "); //%B pour le mois strftime fonction PHP
                echo date('Y'); // pour affficher l'annee 
                ?>.
            </p>
        </h1>

        <!-- fin html -->







        <table class="table table-condensed">

            <?php

            if ($ocoll != null) {
                $i = 0;
                foreach ($ocoll as $omedicament) // affichage des medocs du mois
                {

                    if (date('Ym') == $omedicament->anneeMois) // y pour l'annee, m pour le mois
                    // si anneemois du medicament (dans la bdd) = l'année+mois irl alors on affiche


                    {  // début if : si condition valide on affiche

                    $i++;
            ?>
                        <tr class="ligneTabVisitColor">

                            <td><img style="width:200px;height:200px" src="<?php echo $omedicament->image ?>"></td>
                            <td><?php echo $omedicament->designation_med ?></td>
                            <td><br><button type="button" onclick="affiche_desc('<?php echo $omedicament->desc_detaille ?>')">Description détaillée</button><br><br>
                                <a href="<?php echo "http://localhost/PROJET_PHP_01/PROJET_PHP_01/affiche_noteVisit.php?id_visit=" . $oemploye->id . "&id_med=" . $omedicament->id_med . "" ?>" target="_blank">
                                    <h4 id="note">Note Perso</h4>
                                </a>
                            </td>
                        </tr>

            <?php

                    } 
                }

                if($i==0){
                    echo 'Aucun médicament à présenter pour ce mois-ci.';
                }
            }
            ?>


        </table>
    </div>

    <script src="includes/script.js"></script>
</body>



<?php
if (isset($_GET['sSuccessMsg'])) //vérifie si le booléen existe, (et il existe bien si on a cliqué sur le bouton sauvegarder note, auquel cas on le récupère en get càd dans l'url direct) et affiche l'alerte
{
    echo "<script>alert('Votre note a bien été modifiée !')</script>";
}
?>

</html>