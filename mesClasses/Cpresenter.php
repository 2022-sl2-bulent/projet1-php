<?php

/* fichier de création de la Classe de base métier CmedaPresenter et sa Classe de contrôle CmedaPresenters */


require_once 'mesClasses/Cdao.php'; 
require_once 'mesClasses/Cmedicaments.php';


class CmedaPresenter extends Cmedicament   //héritage pour éviter de tout redéclarer (un médicament à présenter est un type de medicament).

{
    public $anneMois; //on ajoute un attribut de plus par rapport à la classe d'où on hérite

    function __construct($sid_med, $sdesignation_med, $simage, $sdesc_detaille, $sanneeMois)
    {
        parent:: __construct($sid_med, $sdesignation_med, $simage, $sdesc_detaille);
        $this->anneeMois = $sanneeMois;
    }
}

class CmedaPresenters //classe de contrôle de la classe du haut
{
    public $ocollmedicament;

    public function __construct()
    { // constructeur de la classe de contrôle
        $oemploye = unserialize($_SESSION['visitauth']);
        try
        {
            $query = "SELECT * from presenter inner join medicament on medicament.id_med = presenter.id_med WHERE id_visit ='".$oemploye->id."'"; // pour récupèrer que les médicaments du visiteur avec lequel on est connecté
            $odao = new Cdao();
            $lesMedicaments = $odao->gettabDataFromSql($query);
                            
            foreach ($lesMedicaments as $unMedicament)  //on va parcourir ce foreach pour mettre les medocs du mois dans la collection
            {
                $omedicaments = new CmedaPresenter($unMedicament['id_med'],$unMedicament['designation_med'],$unMedicament['image'],$unMedicament['desc_detaille'],$unMedicament['anneeMois']);
                $this->ocollmedicament[] = $omedicaments;
                
            }
        }
        catch(PDOException $e) {
            $msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
            die($msg);
        }
    } // fin constructeur

    public function GetPresentersByAnneeMois()
    {
        $ocollRetour = [];

        foreach($this->ocollmedicament as $unPresenter)
        {
            if($unPresenter->anneeMois == date('Ym'))
            {
                
            }
        }

        return $ocollRetour;
    }
}




?>