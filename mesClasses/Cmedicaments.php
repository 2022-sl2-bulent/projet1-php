<?php

/* fichier de création de la Classe de base métier Cmedicament et sa Classe de contrôle Cmedicaments */

require_once 'mesClasses/Ctri.php';
require_once 'mesClasses/Cdao.php';


class Cmedicament // je crée la classe métier medicament (classe de base, avec 6 attributs)
{
    public $id_med;
    public $designation_med;
    public $image;
    public $desc_detaille;
   

    
    // constructeur (fonction qui va être appélée lors de l'instanciation de la classe métier pour créer un nouvel objet).
    function __construct($sid_med, $sdesignation_med, $simage, $sdesc_detaille) //s pour send param envoyé
    
    {
        $this->id_med = $sid_med;
        $this->designation_med = $sdesignation_med;
        $this->image = $simage;
        $this->desc_detaille = $sdesc_detaille;
    }

}

class Cmedicaments // création de la classe de contrôle (qui va contenir les objets de la classe métier dans une collection)
{
    
    public $ocollmedicament;  // la collection qui va contenir tous les objets

    public function __construct()
    { // constructeur classe controle
        try
        {
            $query = "SELECT * from medicament"; // avec ces 3 lignes on récupère les données de la table "medicament".
            $odao = new Cdao();
            $lesMedicaments = $odao->gettabDataFromSql($query);
                            
                foreach ($lesMedicaments as $unMedicament) //on va parcourir ce foreach pour mettre tous les medocs dans la collection
                {
                    $omedicaments = new Cmedicament($unMedicament['id_med'],$unMedicament['designation_med'],$unMedicament['image'],$unMedicament['desc_detaille']);
                    $this->ocollmedicament[] = $omedicaments;
                }

        }
                  catch(PDOException $e) {
                         $msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
                         die($msg);
                        }
    } // fin constructeur

    function getMedicamentsTrie() // on trie les medicaments par id_med (ou ce qu'on veut)
    {
        $otrie = new Ctri();
        $ocollMedicamentsTrie = $otrie->TriTableau($this->ocollmedicament,'id_med');
        return $ocollMedicamentsTrie;
    }

    /* function GetMedaPresenter() // fonction pour l'affichage des medocs par mois =>> à supp car c'est deja dans le bon fichier (Cpresenter)
    {
        $oemploye = unserialize($_SESSION['visitauth']);
        $query = "SELECT * from presenter inner join medicament on medicament.id_med = presenter.id_med WHERE id_visit ='".$oemploye->id."'"; // retourne les medicaments liés au visiteur actuellement connecté
        $odao = new Cdao();
        $ListMed = $odao->gettabDataFromSql($query);

        return $ListMed;
    } */

    function ajoutMed($sDesignation, $sImageLien, $sDescription)
    {
        $odao = new Cdao();
        $query = "INSERT INTO `medicament` (`designation_med`, `image`, `desc_detaille`) VALUES ('".$sDesignation."', '".$sImageLien."', '".$sDescription."');";
        $odao->insert($query);
    }

    function suppMed($sId_med)
    {
        $odao = new Cdao();
        $query = "DELETE FROM `medicament` WHERE `medicament`.`id_med` =".$sId_med;
        $odao->insert($query);
    }
}