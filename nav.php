<!-- la nabvar = le menu -->

<?php 
session_start();

$ovisiteurConnecte = unserialize($_SESSION['visitauth']); // le visiteur est sérialisé dans form_login de base et on désérialise pour le récup (btw on peut désérialiser à chaque début de fichier où on veut quand on a besoin)
require_once 'includes/head.php'; 
require_once './mesClasses/Cvisiteurs.php';
?>



<link rel="stylesheet" href="nav.css">

<ul>
  
  <li><a href="liste_visiteur.php">Liste Commercial</a></li>
  <li><a href="liste_medicament.php">Liste Médicament</a></li>
  <li><a href="medicamentsMois.php">Médicaments à présenter</a></li>

  <?php
  if($ovisiteurConnecte->role == "ADMIN") // ici si le visiteur co a pour role "ADMIN" alors dans la navbar on affiche Gestion médicaments, sinon pour les autres non
  { ?> 
  <li><a href="ajoutSuppMed.php">Gestion médicaments</a></li> 
  <?php } ?> 

  <li><a href="deconnexion.php">Déconnexion</a></li>

 <!-- <li style="float:right"><a class="active" href="https://www.reseaucerta.org/content/contexte-%C2%AB-laboratoire-gsb-%C2%BB">-----------GSB-----------</a></li> !-->
  
</ul>