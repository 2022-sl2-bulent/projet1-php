<!DOCTYPE html>
<!-- Page de connexion / où on apelle form_login qui lui va gérer la "vraie connexion" -->


<html> 


    <?php 
        require_once 'includes/head.php'; // appelle head.php qui gére le style de la page
        
        session_start(); // = Démarre une nouvelle session ou reprend une session existante
    ?>

    
    <body>
        <div class='container'> <!-- div class pour le style, visible dans le css -->
            
            <?php
            
                
                $formAction = $_SERVER['PHP_SELF']; //Variables de serveur et d'exécution => what is the utility ?

                
                require_once 'includes/form_login_.php'; // appel form_login qui va gérer la connexion avec son formulaire etc
                
            ?>   
            
            <br>
            <br>
            
            <?php
                require_once 'includes/gestion-erreur.php';
                require_once 'includes/footer.php'
            ?>
            
        </div>
    </body>


</html>
