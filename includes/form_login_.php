<!-- le formulaire de connexion -->
<!-- toute la connexion et le passage est géré ici, en POST après un formulaire -->

  
<header title="formlogin"> <!-- le haut de page qu'on gère dans style.css -->
    <h2 title="cnx">Connexion lab GSB</h2>
</header>


<form  class="form-horizontal formLogin" role='form' method = "POST" action=""> <!-- le formulaire -->
    
   
    <div class="<?=!empty($errorMsg)?'form-group has-error':'form-group'?>"> <!-- gère juste le style du msg d'erreur (avec du bootstrap) -->
      <label class="control-label col-sm-5" for="username">Login:</label>
      <div class="col-sm-3">
          <input type="text" class="form-control" id="username" placeholder="Saisir un login" name="username" required="">
      </div>
    </div>
    <div class="<?=!empty($errorMsg)?'form-group has-error':'form-group'?>">
      <label class="control-label col-sm-5" for="pwd">Mot de passe:</label>
      <div class="col-sm-3">          
          <input type="password" class="form-control" id="pwd" placeholder="Saisir un mot de passe" name="pwd" required="">
      </div>
    </div>    
    <div class="form-group">        
      <div class="col-sm-offset-5 col-sm-2">
        <input type="submit" />
      </div>
    </div>
    
</form>


<?php //passage des données du formulaire en POST       
        require_once './mesClasses/Cvisiteurs.php';   
                
        if(isset($_POST['username']) && isset($_POST['pwd']))
        {
            $lesVisiteurs = new Cvisiteurs();
            $ovisiteur = $lesVisiteurs->verifierInfosConnexion($_POST['username'], $_POST['pwd']); //appel la fonction qui va vérifier si le nom et le mdp saisis correspondent à ceux du visiteur dans la bdd
            print_r($ovisiteur);

            if($ovisiteur) // si nom et mdp correct > ça go into location (=connexion) // et on serialise (stock) le visiteur vérifié pour le récuperer ensuite.
            {
                header('Location: liste_medicament.php');
                $_SESSION['visitauth'] = serialize($ovisiteur);
            }
            else
            {
               $errorMsg = "Login/Mot de passe incorrect"; // sinon msg d'erreur
            }            
        }
        
    ?>  
    

