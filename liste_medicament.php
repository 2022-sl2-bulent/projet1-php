<html>

<!-- gère l'affichage de tous les médicaments -->


<?php
require_once 'includes/head.php'; // c'est dans ce fichier qu'on a le lien vers style.css
require_once './mesClasses/Cmedicaments.php';
require_once './mesClasses/Cvisiteurs.php';
require_once 'nav.php'; //barre de menu

?>




    <?php 
    $omedicaments = new Cmedicaments();               //je crée un nouvel objet de type Cmedicaments
    $ocoll = $omedicaments->ocollmedicament;    
    ?>

 <!-- html -->
    <div class="container">

        <header title="listevisiteur"></header>
        <h1>
            <p title="tabvisiteur">Liste de tous les Médicaments à présenter. </p> 
        </h1>

        <!-- fin html -->




        <table class="table table-condensed">

            <?php

        foreach ($ocoll as $omedicament) // affichage des tous les medocs
        {
        ?>
            <tr class="ligneTabVisitColor"> 

                <td><img style="width:200px;height:200px" src="<?php echo $omedicament->image ?>"></td>
                <td><?php echo $omedicament->designation_med ?></td>
                <td><br><button type="button" onclick="affiche_desc('<?php echo $omedicament->desc_detaille ?>')">Description détaillée</button><br><br>
            </tr>
    
            <?php
           
        }?>
   
           
        </table>
    </div>

    <script src="includes/script.js"></script>
</body>

</html>