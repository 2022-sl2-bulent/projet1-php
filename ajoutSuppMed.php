<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajout Supp Med</title>
    <?php
    require_once 'mesClasses/Cmedicaments.php';
    require_once 'includes/head.php';        
    require_once 'nav.php';
    ?>
</head>
<body>
    <?php

    $omedicaments = new Cmedicaments();
    $ovisiteurConnecte = unserialize($_SESSION['visitauth']);


    if($ovisiteurConnecte->role == "ADMIN"){

    ?>


    <div class="container">
        <br>
        <br>

        <div class="ajoutMed">
            <h1>Ajouter un médicament</h1>
            
            <br>

            <form method="post">
                <label for="designationMed">Designation Medicament</label>
                <input name="designationMed">
                
                <br>

                <label for="imageMed">Chemin image</label>
                <input name="imageMed">

                <br>

                <label for="descMed">Description Medicament</label>
                <input name="descMed">

                <br>
                <br>

                <button type="submit" class="btn btn-primary">Ajouter</button>
            </form>

            <?php

            if(isset($_POST['designationMed']) && isset($_POST['imageMed']) && isset($_POST['descMed'])) // à ajouter la condition if visiteur = ADMIN
            {
                $omedicaments->ajoutMed($_POST['designationMed'], $_POST['imageMed'], $_POST['descMed']);
            }

            ?>
        </div>

        <br>
        <br>
        <br>
        <br>
        
        <div class="suppMed">
            <h1>Supprimer un médicament</h1>

            <br>

            <form method="post">
                <select id="choixMed" name="choixMed">
                    <?php
                        foreach($omedicaments->ocollmedicament as $omedicament)
                        {
                        echo "<option value=".$omedicament->id_med.">".$omedicament->designation_med."</option>";
                        }
                    ?>
                </select>

                <br>
                <br>

                <button type="submit" class="btn btn-warning">Supprimer</button>
            </form>

            <?php

            if(isset($_POST['choixMed']))
            {
                $omedicaments->suppMed($_POST['choixMed']);
            }

            ?>
        </div>

        <?php

}
else
{
    echo "<h2>ACCESS DENIed (fdp ntlm jte baiz)<h2>";
}
?>
    </div>
</body>
</html>