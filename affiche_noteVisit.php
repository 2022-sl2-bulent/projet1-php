<?php
require_once 'includes/head.php';
require_once './mesClasses/Cdao.php';
require_once './mesClasses/Cnotes.php';

$id_visit = $_GET['id_visit'];
$id_med = $_GET['id_med']; // on récupère l'id visit et l'id med par l'url

$note = null;

$onote = new Cnotes();
$tabNote = $onote->GetNoteVisiteur($id_visit, $id_med); //on récupère la bonne note (du visiteur co & du médicament sur lequel on a cliqué)

foreach($tabNote as $LaNote)  // faire un foreach car la méthode GetNoteVisiteur retourn un tableau contenant uniquement une note, donc on la récupère comme ceci
{
    $note = $LaNote["texte"];
}
$SuccessMsg = false;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Vos notes</title>
</head>


<body>
    <div class="container">
        <form id="zoneText" method="POST">
            <h1>Votre note sur le médicament</h1>
            <br>

            <textarea rows="11" cols="100" maxlength="1000" name="texteNote">
<?php // bien laissé collé à gauche pour éviter les espaces blancs dans l'affichage
if ($note != null || $note != "") {
    echo $note;
} else {
    echo "";
}
?>
</textarea>

            <br>
            <br>

            <button type="submit" id="btnSaveNote" name="btnSaveNote">Sauvegarder</button>
        </form>


        <?php 

        if(isset($_POST["btnSaveNote"])) // = si on a bien cliqué sur le bouton "sauvegarder la note"
        {
            $onote = new Cnotes();
            $onote->SetNoteVisiteur($_POST["texteNote"], $id_visit, $id_med, $note); //on renvoie en post la note dans la bdd via l'appel de la fonction

            $SuccessMsg = true; // ce booléen qui était faux au début devient vrai ici

            header('Location: MedicamentsMois.php?sSuccessMsg='.$SuccessMsg); //puis on retourne à la page des médicaments

            //mais on y retourne en même temps le booléen vrai qu'on va récuperer en get (le point d'interrogation mean passage de paramètre)
            //ce qui va indiquer qu'on a bien cliqué sur le bouton sauvegarder note
        }


        ?>
    </div>
</body>
</html>